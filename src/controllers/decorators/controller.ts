import 'reflect-metadata';
import { AppRouter } from '../../AppRouter';
import { Methods } from './Methods';
import { MetadataKeys } from './MetadataKeys';
import { RequestHandler } from 'express';

function bodyValidators(keys: string): RequestHandler {
	return (req, res, next) => {
		if( !req.body ) {
			res.status(422).send('Request must have a body');
			return;
		}

		for( let key of keys ) {
			if( !req.body[key] ) {
				res.status(422).send(`Request must have a body with ${key}`);
				return;
			}
		}

		next();


	};
}

export function controller(routePrefix: string) {
	return function(target: Function) {

		const router = AppRouter.getInstance();

		for( let key in target.prototype ) {

			const routerHandler = target.prototype[key];
			const path = Reflect.getMetadata(MetadataKeys.Path, target.prototype, key);
			const method: Methods = Reflect.getMetadata(MetadataKeys.Method, target.prototype, key);
			const middlewares = Reflect.getMetadata(MetadataKeys.Middleware, target.prototype, key) || [];
			const requiredBodyProps = Reflect.getMetadata(MetadataKeys.Validator, target.prototype, key) || [];


			if( path ) {
				router[method](
					routePrefix + path,
					...middlewares,
					bodyValidators(requiredBodyProps),
					routerHandler
				);
			}

		}
	};
}